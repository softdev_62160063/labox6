/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nutta
 */
public class Friend implements Serializable {

    private int id;
    private String Name;
    private int age;
    private String tel;
    private static int lastId = 1;

    public Friend(String Name, int age, String tel) {
        this.id = lastId++;
        this.Name = Name;
        this.age = age;
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Friend{" + "id=" + id + ", Name=" + Name + ", age=" + age + ", tel=" + tel + '}';
    }

    public static void main(String[] args) {
        try {
            Friend f1 = new Friend("J", 22, "0123456789");
            Friend f2 = new Friend("M", 21, "0987654321");
            System.out.println(f1.toString());
            System.out.println(f2.toString());

            f1.setAge(-1);
        } catch (Exception ex) {
            System.out.println("Shit or Cheese");
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        if (age < 0) {
            throw new Exception();
        }
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public static int getLastId() {
        return lastId;
    }

    public static void setLastId(int lastId) {
        Friend.lastId = lastId;
    }

}
